#ifndef MENU_HPP
#define MENU_HPP

#include "enums.hpp"
#include "input.hpp"
#include <iostream>
#include <string.h>
#include <vector>

namespace pz
{

/***
 * @brief Интерективное меню со стрелочками.
 * Класс реализует меню, с управлением стрелочками.
 * Также есть гибкая настройка меню, как и стиля так и структуры.
 * 
 * Пример программы с использованием класса:
 * 		#include <iostream>
 * 		#include "menu.hpp"
 * 		
 * 		int main(int argc, const char**argv)
 * 		{
 * 		    pz::Menu menu();
 * 		
 * 		    menu.set_items({"foo 1",
 * 		                    "foo 2",
 * 		                    "foo 3",
 * 		                    "Exit"});
 * 		    menu.style("Menu:", "=>", "<=");
 * 		
 * 		    while (true)
 * 		    {
 * 		        switch (menu.run())
 * 		        {
 * 		        case 1:
 * 		            // processing input for item 1
 * 		            break;
 * 		        case 2:
 * 		            // processing input for item 2
 * 		            break;
 * 		        case 3:
 * 		            // processing input for item 3
 * 		            break;
 * 		        case 4:
 * 		            return EXIT_SUCCESS;
 * 		            break;
 * 		        default:
 * 		            break;
 * 		        }
 * 		    }
 * 		}
 * 
 * Вывод:
 * 		Menu:
 * 		=>Item 1<=
 * 		  Item 2
 * 		  Item 3
 * 		  Item 4
 * 		Управление(↑↓)
 * 		Выбор<Enter>
 */
class Menu
{
  private:
	 bool clear, // очистка экрана перед каждым выводом
		  show; // показывать подсказку в конце меню
	size_t last; // последний выбраный елемент
	std::vector<std::string> menu; // пункты меню
	std::string ptrl, // левый указатель
		ptrr,		  // правый указатель
		caption, // заголовок меню
		prompt; // подсказка

  public:
	Menu(std::initializer_list<const char*>);
	Menu();
	~Menu();

	/***
	 * @brief Запуск цыкла ожидания.
	 * Запускаеться цыкл, пока пользователь не нажмёт на Enter.
	 * 
	 * @return size_t Выбраный пункт меню [1, ...]
	 */
	size_t run();

	/***
	 * @brief Get the last selected object
	 * 
	 * @return size_t Последный выбор пользователя.
	 */
	size_t get_last_selected();

	/***
	 * @brief Очистка экрана перед каждым выводом меню.
	 * Определяет, нужно ли перед каждым выводом, пунктов меню,
	 * очищать экран.
	 * Запрет на очистку экрана, полезен при отладке.
	 * 
	 * @param clear true - Очищать, false - Не очищать
	 */
	void clear_each_iteration(const bool& clear);

	/***
	 * @brief Set the items object
	 * Иницыализироват пункты меню или перезаписать.
	 * 
	 * Пример иницыализации:
	 * menu.set_items(
	 * 	{
	 * 		"Item 1",
     *      "Item 2",
     *      "Item 3",
     *      "Item 4"
	 * 	});
	 */
	void set_items(std::initializer_list<const char*>);
	
	/***
	 * @brief Функция скрывает курсор в терминале.
	 * 
	 * @param flag скрыть курсор - false, показать курсор - true  
	 */
	void show_cursor(bool flag);

	void style(const std::string& caption, const std::string& ptrl, const std::string& ptrr);
	
	void show_prompt(bool show);

	void set_prompt(std::string& prompt);
};

} // namespace pz

#endif // MENU_HPP