#ifndef INPUT_HPP
#define INPUT_HPP

#include <iostream>
#include <sstream>
#include <vector>
#include <limits>
#include <functional>
#include "enums.hpp"
#include <string.h>

#ifdef _WIN64 // для Windows
#include <conio.h>
#endif // _WIN64

#ifdef __linux__ // для Linux
#include <termios.h>
#include <unistd.h>
#endif // __linux__

namespace pz
{

/**
 * @brief Функция ввода и проверки коректности ввода.
 *      Являеться обёрткой для упрощения ввода чесел,
 *      в случае если ввода строки заместь числа, выведет
 *      сообщение и попросит повторить ввод.
 * 
 * @tparam Args Стандартный тип даных, не рекомендуеться 
 *  использовать строки.
 * @tparam T Строковый тип данных.
 * @param args Переменые для ввода
 * @param message Сообщение в случае ошибки
 */
template <typename T, typename... Args>
void input_check(const T &message, Args &... args)
{
	static_assert(std::is_same<T, char>::value || std::is_same<T, std::string>::value, "For message, use string types!");

	std::cout << message;

	while (!(std::cin >> ... >> args))
	{
		std::cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); // очистка буфера ввода

		std::cout << "Error: " << message;
	}

	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); // очистка буфера ввода
}

/**
 * @brief Функция ввода и проверки коректности ввода.
 *  Являеться обёрткой для упрощения ввода чесел,
 *  в случае если ввода строки заместь числа, выведет
 *  сообщение и попросит повторить ввод.
 *
 * Пример ввода длины массива:
 * input_check("\nEnter size of array /> ", size, [](int size)->bool {return size <= 0; } );
 *
 * @tparam Arg Стандартный тип даных, не рекомендуеться
 *  использовать строки.
 * @tparam T Строковый тип данных.
 * @tparam function Ламбда выражение, повторить при условии.
 * @param message Сообщение в случае ошибки
 */
template <typename T, typename Arg, typename _Function>
void input_check(const T &message, Arg &arg, _Function lambda)
{
	static_assert(std::is_same<T, char>::value ||
					  std::is_array<T>::value ||
					  std::is_same<T, std::string>::value,
				  "For message, use string types!");

	do
	{
		std::cout << message;

		while (!(std::cin >> arg))
		{
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); // очистка буфера ввода

			std::cout << "Error: " << message;
		}
	} while (lambda(arg) == true);

	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); // очистка буфера ввода
}

/**
 * @brief Функция ввода и проверки коректности ввода.
 *  Являеться обёрткой для упрощения ввода чесел,
 *  в случае если ввода строки заместь числа, выведет
 *  сообщение и попросит повторить ввод.
 *
 * Пример ввода длины массива:
 * input_check("\nEnter size of array /> ", size, [](int size)->bool {return size <= 0; } );
 *
 * @tparam Arg Стандартный тип даных, не рекомендуеться
 *  использовать строки.
 * @tparam T Строковый тип данных.
 * @tparam function Ламбда выражение, повторить при условии.
 * @param message Сообщение в случае ошибки
 */
template <typename T, typename Arg>
void input_check(const T &message, Arg &arg)
{
	static_assert(std::is_same<T, char>::value ||
					  std::is_array<T>::value ||
					  std::is_same<T, std::string>::value,
				  "For message, use string types!");

	std::cout << message;

	while (!(std::cin >> arg))
	{
		std::cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); // очистка буфера ввода

		std::cout << "Error: " << message;
	}

	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); // очистка буфера ввода
}

/**
 * @brief Функция ввода и проверки коректности ввода.
 *      Являеться обёрткой для упрощения ввода чесел,
 *      в случае если ввода строки заместь числа, выведет
 *      сообщение и попросит повторить ввод.
 * 
 * @tparam Arg Стандартный тип даных, не рекомендуеться 
 *  использовать строки.
 * @tparam T Строковый тип данных.
 * @param arg Тип возращаемого результата
 * @param message Сообщение в случае ошибки
 * @return результат ввода
 * 
 * Пример:
 *		s1->push(pz::input_check<std::string, int>("Enter element /> "));
 */
template <typename T, typename Arg, typename _Function>
const Arg input_check(const T &message, _Function lambda)
{
	static_assert(std::is_same<T, char>::value || std::is_same<T, std::string>::value, "For message, use string types!");

	Arg arg;

	do
	{
		std::cout << message;
		while (!(std::cin >> arg))
		{
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); // очистка буфера ввода

			std::cout << "Error: " << message;
		}
	} while (lambda(arg) == true);

	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); // очистка буфера ввода

	return arg;
}

/**
 * @brief Функция ввода и проверки коректности ввода.
 *      Являеться обёрткой для упрощения ввода чесел,
 *      в случае если ввода строки заместь числа, выведет
 *      сообщение и попросит повторить ввод.
 * 
 * @tparam Arg Стандартный тип даных, не рекомендуеться 
 *  использовать строки.
 * @tparam T Строковый тип данных.
 * @param arg Тип возращаемого результата
 * @param message Сообщение в случае ошибки
 * @return результат ввода
 * 
 * Пример:
 *		s1->push(pz::input_check<std::string, int>("Enter element /> "));
 */
template <typename T, typename Arg>
const Arg input_check(const T &message)
{
	static_assert(std::is_same<T, char>::value || std::is_same<T, std::string>::value, "For message, use string types!");

	Arg arg;

	std::cout << message;

	while (!(std::cin >> arg))
	{
		std::cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); // очистка буфера ввода

		std::cout << "Error: " << message;
	}

	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); // очистка буфера ввода

	return arg;
}

/**
 * Question to user [y/N] ...
 *
 * @return <tt>true</tt> - согласился, <tt>false</tt> - не согласился
 */
const bool answer_check();

/**
 * @brief Функция считывает один символ из стандартного потока
 * ввода, без нажатия на <Enter>.
 * 
 * @return int ASSCI код введеного символа
 */
int _getchar();

void cmd_clear();

#ifdef _WIN64 // для Windows
#endif		  // _WIN64

#ifdef __linux__ // для Linux

/**
 * @brief Функция выводит цветной текст.
 * Функцию следует использовать в операторах вывода таких как std::cout;
 * после вызова функции весь последующий текст в консоле будет
 * отображен в указаном цвете и с указаными ефектами. Текст будет
 * выводится в цвете до тех пор, пока не будет вызвана функция fb_end()
 * Пример реализации:
 *   std::cout << pz::fground(pz::FG_RED) << "Hello World!!!";
 * 
 * @param color Константа из перечисления pz::Foreground. Цвет последующего текста.
 * @param effect Константа из перечисления pz::Effect. Ефекты последующего текста.
 * @return std::string Последовательность управляющих ASSCI символов.
 */
std::string fground(size_t color, size_t effect = pz::Effect::RESET);

/**
 * @brief Функция очищает всё что сделала функция pz::fgroud().
 * После вызова функции, весь последующий текст будет отображен
 * по умолчанию, пропадут все ефекты текта применяемые до етого.
 * Функцию следует использовать в операторах вывода таких как std::cout;
 * Пример реализации:
 *   std::cout <<  << pz::fb_end() << "Hello World!!!";
 * 
 * @return std::string Последовательность управляющих ASSCI символов.
 */
std::string fb_end();

#endif // __linux__

} // namespace pz

#endif // INPUT_HPP