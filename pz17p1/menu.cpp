#include "menu.hpp"

pz::Menu::Menu(std::initializer_list<const char*> items)
{
	for (const auto &item : items)
        this->menu.push_back(item);
    
	prompt = "\n\033[0;35mУправление\033[1;33m(↑↓)\033[0;35m\nВыбор\033[1;33m<Enter>\033[0m";
    last = 0;
	ptrl = "-> ";
	ptrr = " <-";
	caption = "<< М Е Н Ю >>";
	clear = true;
}

pz::Menu::Menu()
{
	prompt = "\n\033[0;35mУправление\033[1;33m(↑↓)\033[0;35m\nВыбор\033[1;33m<Enter>\033[0m";
	last = 0;
	ptrl = "-> ";
	ptrr = " <-";
	caption = "<< М Е Н Ю >>";
	clear = true;
}

void pz::Menu::show_prompt(bool show)
{
	this->show = show;
}

void pz::Menu::set_prompt(std::string& prompt)
{
	this->prompt = prompt;
}

pz::Menu::~Menu()
{
}

size_t pz::Menu::run()
{
    int answer = 0;

    do
    {
        if (clear == true)
			pz::cmd_clear();

        std::cout << this->caption;

        for (size_t i(0); i < menu.size(); i++)
            std::cout << std::endl
                      << ((this->last == i) ? this->ptrl : std::string(ptrl.size(), ' ')) << this->menu.at(i)
                      << ((this->last == i) ? this->ptrr : "");

		if (show == true)
			std::cout << prompt;

		answer = pz::_getchar();

        if (answer == KEY_DOWN && this->last < menu.size())
            this->last++;
        if (answer == KEY_DOWN && this->last == menu.size())
            this->last = 0;
        else if (answer == KEY_UP && this->last > 0)
            this->last--;
        else if (answer == KEY_UP && this->last == 0)
            this->last = menu.size() - 1;

    } while (answer != ENTER);

    return this->last + 1;
}

size_t pz::Menu::get_last_selected()
{
    return this->last + 1;
}

void pz::Menu::clear_each_iteration(const bool& clear)
{
    this->clear = clear;
}

void pz::Menu::show_cursor(bool flag)
{
    system("tput reset ");

    system((flag) ? "tput cnorm" : "tput civis");
}

void pz::Menu::set_items(std::initializer_list<const char*> items)
{
    menu.clear();

    for(const auto &item : items)
        this->menu.push_back(item);
}

void pz::Menu::style(const std::string& caption, const std::string& ptrl, const std::string& ptrr)
{
    this->caption = caption;
    this->ptrl = ptrl;
    this->ptrr = ptrr;
}
